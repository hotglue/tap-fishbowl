from typing import Any, Dict, Optional

import requests

from tap_fishbowl.client_base import FishbowlStream

class FishbowEndPointlStream(FishbowlStream):

    @property
    def url_base(self) -> str:
        instance = self.config.get("instance")
        url = f"http://{instance}/api"
        return url

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Any:
        
        resp = response.json()
        totalPages = resp.get('totalPages')
        pageNumber = resp.get('pageNumber')
        if pageNumber < totalPages: 
            return pageNumber + 1 
        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        params: dict = {}
        params["pageSize"] = 100
        if next_page_token :   
            params["pageNumber"] = next_page_token
        return params
