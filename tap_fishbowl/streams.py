"""Stream type classes for tap-fishbowl."""

from singer_sdk import typing as th

from tap_fishbowl.client_query import FishbowQuerylStream
from tap_fishbowl.client_endpoint import FishbowEndPointlStream

class CustomerStream(FishbowQuerylStream):
    """Define custom stream."""

    name = "customer"
    primary_keys = ["id"]
    replication_key = "dateLastModified"
    table_name = "customer"

    schema = th.PropertiesList(
        th.Property("accountingHash", th.StringType),
        th.Property("issuableStatusId", th.IntegerType),
        th.Property("note", th.StringType),
        th.Property("customFields", th.StringType),
        th.Property("accountingId", th.StringType),
        th.Property("carrierServiceId", th.IntegerType),
        th.Property("number", th.StringType),
        th.Property("defaultPaymentTermsId", th.IntegerType),
        th.Property("dateCreated", th.DateTimeType),
        th.Property("dateLastModified", th.DateTimeType),
        th.Property("lastChangedUser", th.StringType),
        th.Property("creditLimit", th.NumberType),
        th.Property("id", th.IntegerType),
        th.Property("sysUSerId", th.IntegerType),
        th.Property("currencyId", th.IntegerType),
        th.Property("taxRateId", th.IntegerType),
        th.Property("activeFlag", th.BooleanType),
        th.Property("qbClassId", th.IntegerType),
        th.Property("taxExempt", th.BooleanType),
        th.Property("defaultSalesmanId", th.IntegerType),
        th.Property("toBePrinted", th.BooleanType),
        th.Property("parentId", th.IntegerType),
        th.Property("url", th.StringType),
        th.Property("defaultShipTermsId", th.IntegerType),
        th.Property("accountID", th.IntegerType),
        th.Property("defaultCarrierId", th.IntegerType),
        th.Property("defaultPriorityId", th.IntegerType),
        th.Property("statusId", th.IntegerType),
        th.Property("toBeEmailed", th.BooleanType),
        th.Property("name", th.StringType),
        th.Property("currencyRate", th.NumberType),
        th.Property("jobDepth", th.IntegerType),
    ).to_dict()

class ProductStream(FishbowQuerylStream):
    """Define custom stream."""

    name = "product"
    primary_keys = ["id"]
    replication_key = "dateLastModified"
    table_name = "product"

    schema = th.PropertiesList(
        th.Property("accountingHash",th.StringType),
        th.Property("sizeUomId",th.IntegerType),
        th.Property("partId",th.IntegerType),
        th.Property("customFields",th.StringType),
        th.Property("accountingId",th.StringType),
        th.Property("num",th.StringType),
        th.Property("description",th.StringType),
        th.Property("kitFlag",th.BooleanType),
        th.Property("sellableInOtherUoms",th.BooleanType),
        th.Property("dateCreated",th.DateTimeType),
        th.Property("dateLastModified",th.DateTimeType),
        th.Property("usePriceFlag",th.BooleanType),
        th.Property("len",th.NumberType),
        th.Property("price",th.NumberType),
        th.Property("details",th.StringType),
        th.Property("id",th.IntegerType),
        th.Property("incomeAccountId",th.IntegerType),
        th.Property("sku",th.StringType),
        th.Property("defaultCartonTypeId",th.IntegerType),
        th.Property("defaultSoItemType",th.IntegerType),
        th.Property("activeFlag",th.BooleanType),
        th.Property("height",th.NumberType),
        th.Property("cartonCount",th.NumberType),
        th.Property("qbClassId",th.IntegerType),
        th.Property("taxableFlag",th.BooleanType),
        th.Property("kitGroupedFlag",th.BooleanType),
        th.Property("upc",th.StringType),
        th.Property("weight",th.NumberType),
        th.Property("uomId",th.IntegerType),
        th.Property("url",th.StringType),
        th.Property("showSoComboFlag",th.BooleanType),
        th.Property("taxId",th.IntegerType),
        th.Property("displayTypeId",th.IntegerType),
        th.Property("width",th.NumberType),
        th.Property("weightUomId",th.IntegerType),
        th.Property("alertNote",th.StringType),
    ).to_dict()


class SoStream(FishbowQuerylStream):
    """Define custom stream."""

    name = "sales_orders"
    primary_keys = ["id"]
    replication_key = "dateLastModified"
    table_name = "so"

    schema = th.PropertiesList(
        th.Property("shipToStateId",th.IntegerType), 
        th.Property("dateFirstShip",th.DateTimeType),
        th.Property("num",th.StringType),
        th.Property("dateRevision",th.DateTimeType),
        th.Property("revisionNum",th.IntegerType),
        th.Property("subTotal",th.NumberType),
        th.Property("billToCity",th.StringType),
        th.Property("priorityId",th.IntegerType),
        th.Property("mcTotalTax",th.NumberType),
        th.Property("totalTax",th.NumberType),
        th.Property("shipToCity",th.StringType),
        th.Property("billToZip",th.StringType),
        th.Property("dateCalStart",th.DateTimeType),
        th.Property("dateLastModified",th.DateTimeType),
        th.Property("registerId",th.IntegerType),
        th.Property("id",th.IntegerType),
        th.Property("currencyId",th.IntegerType),
        th.Property("customerPO",th.StringType),
        th.Property("taxRateId",th.IntegerType),
        th.Property("locationGroupId",th.IntegerType),
        th.Property("shipTermsId",th.IntegerType),
        th.Property("dateCalEnd",th.DateTimeType),
        th.Property("salesmanId",th.IntegerType),
        th.Property("salesmanInitials",th.StringType),
        th.Property("dateExpired",th.DateTimeType),
        th.Property("calCategoryId",th.IntegerType),
        th.Property("fobPointId",th.IntegerType),
        th.Property("billToCountryId",th.IntegerType),
        th.Property("shipToCountryId",th.IntegerType),
        th.Property("taxRate",th.NumberType),
        th.Property("statusId",th.IntegerType),
        th.Property("phone",th.StringType),
        th.Property("estimatedTax",th.NumberType),
        th.Property("toBeEmailed",th.BooleanType),
        th.Property("salesman",th.StringType),
        th.Property("typeId",th.IntegerType),
        th.Property("note",th.StringType),
        th.Property("shipToAddress",th.StringType),
        th.Property("totalPrice",th.NumberType),
        th.Property("customFields",th.StringType),
        th.Property("carrierServiceId",th.IntegerType),
        th.Property("totalIncludesTax",th.BooleanType),
        th.Property("vendorPO",th.StringType),
        th.Property("shipToResidential",th.BooleanType),
        th.Property("billToAddress",th.StringType),
        th.Property("paymentTermsId",th.IntegerType),
        th.Property("shipToZip",th.StringType),
        th.Property("dateCreated",th.DateTimeType),
        th.Property("customerContact",th.StringType),
        th.Property("datecompleted",th.DateTimeType),
        th.Property("customerId",th.IntegerType),
        th.Property("taxRateName",th.StringType),
        th.Property("email",th.StringType),
        th.Property("createdByUserId",th.IntegerType),
        th.Property("dateIssued",th.DateTimeType),
        th.Property("cost",th.NumberType),
        th.Property("qbClassId",th.IntegerType),
        th.Property("toBePrinted",th.BooleanType),
        th.Property("billToName",th.StringType),
        th.Property("url",th.StringType),
        th.Property("shipToName",th.StringType),
        th.Property("currencyRate",th.NumberType),
        th.Property("carrierId",th.IntegerType),
        th.Property("billToStateId",th.IntegerType),
        th.Property("username",th.StringType),
    ).to_dict()


class InventoryStream(FishbowEndPointlStream):
    name = "inventory"
    path = "/parts/inventory"
    primary_keys = ["id"]
    records_jsonpath = '$.results[*]'

    schema = th.PropertiesList(
        th.Property("id",th.IntegerType),
        th.Property("partNumber",th.StringType),
        th.Property("quantity",th.StringType),
        th.Property("partDescription",th.StringType),
        th.Property("uom",
            th.ObjectType( 
                th.Property("id",th.IntegerType),
                th.Property("name",th.StringType),
                th.Property("abbreviation",th.StringType),
            )
        ),
    ).to_dict()

class AddressStream(FishbowQuerylStream):
    name = "address"
    primary_keys= ["id"]
    table_name = "address"
    schema = th.PropertiesList(
        th.Property("zip",th.StringType),
        th.Property("defaultFlag",th.BooleanType),
        th.Property("address",th.StringType),
        th.Property("city",th.StringType),
        th.Property("residentialFlag",th.BooleanType),
        th.Property("stateId",th.IntegerType),
        th.Property("countryId",th.IntegerType),
        th.Property("accountId",th.IntegerType),
        th.Property("name",th.StringType),
        th.Property("addressName",th.StringType),
        th.Property("typeID",th.IntegerType),
        th.Property("id",th.IntegerType),
        th.Property("locationGroupId",th.IntegerType),
    ).to_dict()

class StateStream(FishbowQuerylStream):
    name = "stateconst"
    primary_keys= ["id"]
    table_name = "stateconst"
    schema = th.PropertiesList(
        th.Property("countryConstID",th.IntegerType),
        th.Property("code",th.StringType),
        th.Property("name",th.StringType),
        th.Property("id",th.IntegerType),
    ).to_dict()

class CountryStream(FishbowQuerylStream):
    name = "countryconst"
    primary_keys= ["id"]
    table_name = "countryconst"
    schema = th.PropertiesList(
        th.Property("name",th.StringType),
        th.Property("id",th.IntegerType),
        th.Property("abbreviation",th.StringType),
    ).to_dict()

class LocationGroupStream(FishbowQuerylStream):
    name = "locationgroup"
    primary_keys= ["id"]
    table_name = "locationgroup"
    schema = th.PropertiesList(
        th.Property("dateLastModified",th.DateTimeType),
        th.Property("qbClassId",th.IntegerType),
        th.Property("name",th.StringType),
        th.Property("id",th.IntegerType),
        th.Property("activeFlag",th.BooleanType),
    ).to_dict()

class PaymentTermsStream(FishbowQuerylStream):
    name = "paymentterms"
    primary_keys = ["id"]
    table_name = 'paymentterms'
    schema = th.PropertiesList(
        th.Property("id",th.IntegerType),
        th.Property("typeId",th.IntegerType),
        th.Property("name",th.StringType),
        th.Property("discount",th.NumberType),
    ).to_dict()

class PriorityStream(FishbowQuerylStream):
    name = 'priority'
    primary_keys = ["id"]
    table_name = 'priority'
    schema = th.PropertiesList(
        th.Property('name',th.StringType),
        th.Property('id',th.IntegerType),
    ).to_dict()

class CustomerStateStream(FishbowQuerylStream):
    name = "customerstatus"
    table_name = "customerstatus"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("name",th.StringType),
        th.Property("id",th.IntegerType),
    ).to_dict()

class CustomerTypeStream(FishbowQuerylStream):
    name = "customerincltype"
    table_name = "customerincltype"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("name",th.StringType),
        th.Property("id",th.IntegerType),
    ).to_dict()

class SaleOrderItemsStream(FishbowQuerylStream):
    name = "soitem"
    table_name = "soitem"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("note",th.StringType),
        th.Property("qtyFulfilled",th.NumberType),
        th.Property("totalPrice",th.NumberType),
        th.Property("customFields",th.StringType),
        th.Property("adjustPercentage",th.NumberType),
        th.Property("description",th.StringType),
        th.Property("dateScheduledFulfillment",th.DateTimeType),
        th.Property("dateLastModified",th.DateTimeType),
        th.Property("id",th.IntegerType),
        th.Property("mcTotalPrice",th.NumberType),
        th.Property("showItemFlag",th.BooleanType),
        th.Property("qtyPicked",th.NumberType),
        th.Property("unitPrice",th.NumberType),
        th.Property("soId",th.IntegerType),
        th.Property("qbClassId",th.IntegerType),
        th.Property("productId",th.IntegerType),
        th.Property("exchangeSOLineItem",th.StringType),
        th.Property("taxableFlag",th.BooleanType),
        th.Property("adjustAmount",th.NumberType),
        th.Property("dateLastFulfillment",th.DateTimeType),
        th.Property("uomId",th.IntegerType),
        th.Property("productNum",th.StringType),
        th.Property("customerPartNum",th.StringType),
        th.Property("markupCost",th.NumberType),
        th.Property("taxRate",th.NumberType),
        th.Property("revLevel",th.StringType),
        th.Property("statusId",th.IntegerType),
        th.Property("qtyOrdered",th.NumberType),
        th.Property("taxId",th.IntegerType),
        th.Property("qtyToFulfill",th.NumberType),
        th.Property("typeId",th.IntegerType),
        th.Property("soLineItem",th.IntegerType),
        th.Property("totalCost",th.NumberType),
    ).to_dict()

class AddressTypeStream(FishbowQuerylStream):
    name = "addresstype"
    table_name = "addresstype"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("name",th.StringType),
        th.Property("id",th.IntegerType),
    ).to_dict()

class AccountGroupStream(FishbowQuerylStream):
    name = "accountgroup"
    table_name = "accountgroup"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("name",th.StringType),
        th.Property("id",th.IntegerType),
    ).to_dict()

class AccountGroupRelationStream(FishbowQuerylStream):
    name = "accountgrouprelation"
    table_name = "accountgrouprelation"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("accountId",th.IntegerType),
        th.Property("groupId",th.IntegerType),
        th.Property("id",th.IntegerType),
    ).to_dict()

class ProductTreeStream(FishbowQuerylStream):
    name = "producttree"
    table_name = "producttree"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("name",th.StringType),
        th.Property("id",th.IntegerType),
        th.Property("parentId",th.IntegerType),
        th.Property("description",th.StringType),
    ).to_dict()

class ContactStream(FishbowQuerylStream):
    name = "contact"
    table_name = "contact"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("accountId",th.IntegerType),
        th.Property("defaultFlag",th.BooleanType),
        th.Property("contactName",th.StringType),
        th.Property("datus",th.StringType),
        th.Property("typeId",th.IntegerType),
        th.Property("id",th.IntegerType),
        th.Property("addressId",th.IntegerType)
    ).to_dict()

class ImagesStream(FishbowQuerylStream):
    name = "images"
    table_name = "image"
    primary_keys = ["id"]
    page_size = 2
    schema = th.PropertiesList(
        th.Property("recordId",th.IntegerType),
        th.Property("id",th.IntegerType),
        th.Property("type",th.StringType),
        th.Property("imageFull",th.StringType),
        th.Property("imageThumbnail",th.StringType),
        th.Property("tableName",th.StringType),
    ).to_dict()

class PricingRuleStream(FishbowQuerylStream):
    name = "pricing_rule"
    table_name = "pricingrule"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("spcGetYFree",th.IntegerType),
        th.Property("rndPMAmount",th.NumberType),
        th.Property("customerInclTypeId",th.IntegerType),
        th.Property("rndApplies",th.BooleanType),
        th.Property("spcBuyX",th.IntegerType),
        th.Property("description",th.StringType),
        th.Property("dateEnd",th.DateTimeType),
        th.Property("paBaseAmountTypeId",th.IntegerType),
        th.Property("isActive",th.BooleanType),
        th.Property("rndToAmount",th.NumberType),
        th.Property("dateApplies",th.BooleanType),
        th.Property("dateBegin",th.DateTimeType),
        th.Property("qtyMin",th.NumberType),
        th.Property("dateCreated",th.DateTimeType),
        th.Property("dateLastModified",th.DateTimeType),
        th.Property("paPercent",th.NumberType),
        th.Property("id",th.IntegerType),
        th.Property("productInclId",th.IntegerType),
        th.Property("qtyMax",th.NumberType),
        th.Property("rndTypeId",th.IntegerType),
        th.Property("paApplies",th.BooleanType),
        th.Property("userId",th.IntegerType),
        th.Property("isAutoApply",th.BooleanType),
        th.Property("paTypeId",th.IntegerType),
        th.Property("spcApplies",th.BooleanType),
        th.Property("paAmount",th.NumberType),
        th.Property("productInclTypeId",th.IntegerType),
        th.Property("name",th.StringType),
        th.Property("isTier2",th.BooleanType),
        th.Property("qtyApplies",th.BooleanType),
        th.Property("rndIsMinus",th.BooleanType),
        th.Property("customerInclId",th.IntegerType),
    ).to_dict()


class PartStream(FishbowQuerylStream):
    """Define custom stream."""

    name = "part"
    primary_keys = ["id"]
    replication_key = "dateLastModified"
    table_name = "part"

    # Didn't add all the fields from this table. 

    schema = th.PropertiesList(
        th.Property("id",th.IntegerType),
        th.Property("defaultProductId",th.IntegerType),
        th.Property("dateLastModified",th.DateTimeType),
        th.Property("stdCost",th.NumberType),
        th.Property("num",th.StringType),
        th.Property("description",th.StringType),
    ).to_dict()