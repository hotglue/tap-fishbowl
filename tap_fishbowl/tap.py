"""Fishbowl tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_fishbowl.streams import ( CustomerStream, 
                                    ProductStream,
                                    SoStream,
                                    InventoryStream,
                                    AddressStream,
                                    StateStream,
                                    CountryStream,
                                    LocationGroupStream,
                                    CustomerStateStream,
                                    PriorityStream,
                                    PaymentTermsStream,
                                    SaleOrderItemsStream,
                                    AddressTypeStream,
                                    ContactStream,
                                    ImagesStream,
                                    AccountGroupStream,
                                    AccountGroupRelationStream,
                                    PricingRuleStream,
                                    PartStream)

STREAM_TYPES = [CustomerStream,
                ProductStream,
                SoStream,
                InventoryStream,
                AddressStream,
                StateStream,
                CountryStream,
                LocationGroupStream,
                CustomerStateStream,
                PriorityStream,
                PaymentTermsStream,
                SaleOrderItemsStream,
                AddressTypeStream,
                ContactStream,
                ImagesStream,
                AccountGroupStream,
                AccountGroupRelationStream,
                PricingRuleStream,
                PartStream]


class TapFishbowl(Tap):
    """Fishbowl tap class."""

    name = "tap-fishbowl"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        self.sql_merge = None
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_token",
            th.StringType,
            required=False,
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "instance",
            th.StringType,
            description="The URL instance for API's base_url",
            required=True,
        ),
        th.Property(
            "app_name",
            th.StringType,
            description="The App name registered and approved in the Fishbowl Client",
            required=True,
        ),
        th.Property("app_id", 
            th.StringType, 
            description="App id", 
            required=True
        ),
        th.Property(
            "username",
            th.StringType,
            description="Registered username on the Fishbowl's server",
            required=True,
        ),
        th.Property(
            "password", th.StringType, description="username's password", required=True
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapFishbowl.cli()
