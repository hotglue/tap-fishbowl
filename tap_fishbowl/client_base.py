from singer_sdk.streams import RESTStream

from tap_fishbowl.auth import Authenticator

class FishbowlStream(RESTStream):
    """Fishbowl stream class."""

    records_jsonpath = "$[*]"

    @property
    def url_base(self) -> str:
        instance = self.config.get("instance")
        url = f"http://{instance}/api"
        return url

    @property
    def authenticator(self) -> Authenticator:
        """Return a new authenticator object."""
        url = f"{self.url_base}/login"
        return Authenticator(self, self._tap.config, url)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {"content-type": "application/sql"}
        return headers

    def __del__(self):
        self.authenticator.logout()