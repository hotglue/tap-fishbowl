from typing import Any, Dict, Optional, cast

import requests

from tap_fishbowl.client_base import FishbowlStream

class FishbowQuerylStream(FishbowlStream):

    path = "/data-query"
    page_size = 1000
    rest_method = "GET"

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        if response.json():
            if not previous_token:
                return self.page_size
            else:
                return self.page_size + previous_token

        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        return params

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:

        filters = []
        order_by = ""
        time_format = "'%Y-%m-%d %H:%M:%S'"

        if self.replication_key:
            order_by = f"ORDER BY {self.replication_key}"

            start_date = self.get_starting_timestamp(context)

            if start_date:
                start_date_str = start_date.strftime(time_format)
                filters.append(f"{self.replication_key}>{start_date_str}")

        if filters:
            filters = "WHERE " + " AND ".join(filters)
        else:
            filters = ""

        selected_properties = []
        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                field_name = f"{field_name}"
                selected_properties.append(field_name)

        select = ", ".join(selected_properties)

        if next_page_token:

            payload = f"SELECT {select} FROM {self.table_name} {filters} {order_by} LIMIT {self.page_size} OFFSET {next_page_token}"
        else:
            payload = f"SELECT {select} FROM {self.table_name} {filters} {order_by} LIMIT {self.page_size}"

        return payload

    def prepare_request(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> requests.PreparedRequest:

        http_method = self.rest_method
        url: str = self.get_url(context)
        params: dict = self.get_url_params(context, next_page_token)
        request_data = self.prepare_request_payload(context, next_page_token)
        headers = self.http_headers

        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})
            params.update(authenticator.auth_params or {})

        request = cast(
            requests.PreparedRequest,
            self.requests_session.prepare_request(
                requests.Request(
                    method=http_method,
                    url=url,
                    params=params,
                    headers=headers,
                    data=request_data,
                ),
            ),
        )
        return request
