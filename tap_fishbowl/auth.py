import json
from datetime import datetime
from typing import Optional

import requests
from singer_sdk.authenticators import APIAuthenticatorBase
from singer_sdk.streams import Stream as RESTStreamBase


class Authenticator(APIAuthenticatorBase):
    def __init__(
        self,
        stream: RESTStreamBase,
        config_file: Optional[str] = None,
        auth_endpoint: Optional[str] = None,
    ) -> None:
        super().__init__(stream=stream)
        self._auth_endpoint = auth_endpoint
        self._config_file = config_file
        self._tap = stream._tap
        self._base_url = stream.url_base
        self.token = self._tap._config.get('auth_token')

    @property
    def auth_headers(self) -> dict:
        if not self.is_token_valid():
            self.update_access_token()
        result = super().auth_headers
        result["Authorization"] = f"Bearer {self.token}"
        return result

    @property
    def oauth_request_body(self) -> dict:

        return {
            "appName": self._tap._config["app_name"],
            "appId": int(self._tap._config["app_id"]),
            "username": self._tap._config["username"],
            "password": self._tap._config["password"],
        }

    def is_token_valid(self) -> bool:
        
        if not self.token:
            return False

        last_login = self._tap.config.get("last_login")

        if not last_login:
            return False

        now = round(datetime.utcnow().timestamp())

        last_login = round(
            datetime.strptime(
                last_login, "%Y-%m-%dT%H:%M:%S"
            ).timestamp()
        )

        return not ((last_login + 3600) - now < 120)

    @property
    def auth_endpoint(self) -> str:

        if not self._auth_endpoint:
            raise ValueError("Authorization endpoint not set.")
        return self._auth_endpoint

    @property
    def oauth_request_payload(self) -> dict:

        return self.oauth_request_body

    # Logs out from Fishbowl Server
    def logout(self) -> None:
        requests.post(
            url=f"{self._base_url}/logout",
            headers={
                "Content-Type": "application/json",
                "Authorization": f"Bearer {self.token}",
            },
        )

        # updates the config file 
        self._tap._config["auth_token"] = None
        self._tap._config["last_login"] = None

        # writes the new token and the last login in the config file
        # for future use
        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)

    # Authentication and refresh
    def update_access_token(self) -> None:

        # logs out before getting a new token
        self.logout()

        # prepares the request for the new token 
        auth_request_payload = self.oauth_request_payload
        token_response = requests.post(
            url=self.auth_endpoint, json=auth_request_payload
        )

        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.json()}'. {ex}"
            )
        
        # saves new token into self.token
        token_json = token_response.json()
        self.token = token_json["token"]

        # updates the config file 
        self._tap._config["auth_token"] = self.token
        self._tap._config["last_login"] = datetime.utcnow().strftime(
            "%Y-%m-%dT%H:%M:%S"
            )

        # writes the new token and the last login in the config file
        # for future use
        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)

    def __exit__(self):
        self.logout()